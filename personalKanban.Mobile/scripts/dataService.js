var dataService = function(){
    
    
    //var serviceBase = 'http://localhost:3000/';
    var serviceBase = 'http://kanbanmobileservice.azurewebsites.net/';
    login = function(accountName , password, callback){
            $.post( serviceBase + 'login',
                {
                    name: accountName,
                    email: "",
                    password: password
                },
                function(data) { },
                'json'
                )
                .success(function(data) {
                    callback('success',data);
                })
                .error(function(xhr, textStatus, errorThrown) { 
                    callback('failure',xhr.responseText); 
                }
            );
        
    },
    createUser = function(accountName , email, password, callback){
        $.post( serviceBase + 'createUser',
            {
                name: accountName,
                email: email,
                password: password
            },
            function(data) { },
            'json'
            )
            .success(function() {
                callback('success','test');
            })
            .error(function(xhr, textStatus, errorThrown) { 
                callback('failure',xhr.responseText); 
            }
        );
    },
    getApi = function(callback){
         $.getJSON(serviceBase + 'api', function(result,err) {
             callback(result);
        });
    }
    getBoards = function(userId,callback){ //alert('b4 called');
        $.ajaxSetup({ cache: false });
        $.getJSON(serviceBase + userId + '/boards', function(result,status,err) {
            // alert('called');   
            var status = 'success';
            if(status != 'success'){
                console.log('[Error]dataService.getBoards-' + err);
                status = 'failure';
            }
            callback(result,status);
        });
    },
    insertBoard = function(userId,title,callback){
        $.post(serviceBase + userId + '/boards',
            {
                title: title,
            },
            function(data) { },
            'json'
            )
            .success(function() {
                callback('success','');
            })
            .error(function(xhr, textStatus, errorThrown) { 
                callback('failure',xhr.responseText); 
            }
        );
    },
    deleteBoard = function(userId,title,callback){
        $.ajax({
            url:serviceBase + userId + '/boards/' + title,
            type : 'DELETE',
            success :function() {
                alert('test');
                callback('success','');
            },
            error :function(xhr, textStatus, errorThrown) { 
                callback('failure',xhr.responseText); 
            } 
        });
    },
    getTables = function(userId,boardTitle,callback){ 
        $.ajaxSetup({ cache: false });
        $.getJSON(serviceBase + userId + '/tables/' + boardTitle, function(result,status,err) {  
           // var status = 'success';
            if(status != 'success'){
                console.log('[Error]dataService.getJSON-' + err);
                status = 'failure';
            }
            callback(result,status);
        });
    },
    updateTables = function(userId,boardTitle,tables,callback){ 
        $.ajaxSetup({ cache: false });
        $.post(serviceBase + userId + '/tables/' + boardTitle,
            {
                tables: tables.toJSON(),
            },
            function(data) { },
            'json'
            )
            .success(function() {
                callback('success','');
            })
            .error(function(xhr, textStatus, errorThrown) { 
                callback('failure',xhr.responseText); 
            }
        );
    },
    addCard = function(userId,boardTitle,tableName, cardObject, callback){
        $.ajaxSetup({ cache: false });
        $.post(serviceBase + userId + '/card/',
            {
               boardTitle: boardTitle,
               tableName: tableName,
               cardObject: cardObject,
            },
            function(data) { },
            'json'
            )
            .success(function() {
                callback('success','');
            })
            .error(function(xhr, textStatus, errorThrown) { 
                callback('failure',xhr.responseText); 
            }
        );
    },
    getCardDetails = function(userId, boardTitle, cardId, callback){
        $.ajaxSetup({ cache: false });
        $.getJSON(serviceBase + userId + '/card/' + cardId, function(result,status,err) {  
            if(status != 'success'){
                console.log('[Error]dataService.getJSON-' + err);
                status = 'failure';
            }
            callback(result,status);
        });
    }
    return {
        getCardDetails : getCardDetails,
        addCard : addCard,
        updateTables : updateTables,
        getTables : getTables,
        login: login,
        createUser : createUser,
        getApi : getApi,
        getBoards : getBoards,
        insertBoard: insertBoard,
        deleteBoard : deleteBoard,
    };
}();