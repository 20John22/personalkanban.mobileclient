// JavaScript Document

// Wait for PhoneGap to load
document.addEventListener("deviceready", onDeviceReady, false);

// PhoneGap is ready
function onDeviceReady() {

}

function getQueryVariable(variable) { 
      var query = window.location.hash.split('?')[1]; 
      var vars = query.split("&"); 
      for (var i=0;i<vars.length;i++) { 
        var pair = vars[i].split("="); 
        if (pair[0] == variable) { 
          return pair[1]; 
        } 
      }
} 

function onCardViewInit(e){
    var boardName = getQueryVariable('card');
    cardDetailsViewModel.getCardDetails(boardName);
    
}



function initBoardData(e){
    var boardName = getQueryVariable('boardName');
    var boardTitle = document.getElementById('boardTitle');
    boardTitle.innerHTML  = boardName;
    
    boardViewModel.set('boardTitle',boardName);
    boardViewModel.getTables();
    
}

//=======================Route actions=======================//
function addCard(e)
{
    e.preventDefault();
    var tableName = getQueryVariable('table');
    /*
    var cardName = document.getElementById('txtCardName').value;
    var cardDetails1 = document.getElementById('txtCardDetails').value;
    var cardDetails2 = document.getElementById('txtCardDetails2').value;
    cardObject = {
        cardName: cardName,
        cardDetails1 : cardDetails1,
        cardDetails2 : cardDetails2
    }
    */
    cardDetailsViewModel.addCard(tableName,function(){
        app.navigate("#:back"); //?boardName=" + boardsViewModel.get("boardTitle")); 
    });
}

function deleteBoard(event){
    var boardTitle = event.target.attr('delete-element');
    boardsViewModel.deleteBoard(boardTitle,function(){
        boardsViewModel.initBoards();
           app.navigate("#boards"); 
    });
}


function createBoard(e)
{
    e.preventDefault();
    var title = document.getElementById('txtBoardTitle').value;
    boardsViewModel.insertBoard(title,function(){
        app.navigate("#boards"); 
    });
}


function login() {
 
    loginViewModel.login(function(isValid){
        if(isValid){
            boardsViewModel.initBoards();
            app.navigate("#boards"); 
        }
    });
   
}

function registerUser(){
    loginViewModel.createUser(function(isCreatedUser){
        if(isCreatedUser){
            boardsViewModel.initBoards();
            app.navigate("#boards"); 
        }
    });
}