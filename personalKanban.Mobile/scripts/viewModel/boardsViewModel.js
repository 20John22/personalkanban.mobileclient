/// <reference path="~/scripts/dataService.js"> 
/// <reference path="~/scripts/viewModel/loginViewModel.js"> 

var boardsViewModel = kendo.observable({
    boards : [],
    initBoards: function(){
           
        dataService.getBoards(loginViewModel.get("id"), function(data,status){
                
            if(status == 'failure'){
                alert('No data');
            }
            else{
                /*
                var boardsArray = [];
                foreach (var item in data) {
                    alert(item);
                }*/
                var mapedData = data.map(function(item){
                    return { Title : item.Title };
                }); 
              //  var dataSource = new kendo.data.DataSource.create({ data: mapedData });
                boardsViewModel.set('boards', mapedData);
                
            }
        });
    },
    isEmpty: function(val){
        return (val === undefined || val == null || val.length <= 0) ? true : false;
    },
    isValidBoard: function(){
        return loginViewModel.get("id") == 0;
    },
    isValidTitle: function(newTitle){
        //var item = '';
        for(var i in this.boards) {
            if(this.boards[i].Title == newTitle){
                return false;
            }   
        }
        return true;
    },
    getBoards : function(callback){
        return this.get('boards');
    },
    insertBoard: function(title,callback){
        if(this.isEmpty(title))
        {
            alert('Can not create empty without name.');
            return false;
        }
        if(!this.isValidTitle(title)){
            alert('Board with same title already exsists.');
            return false;
        }
        dataService.insertBoard(loginViewModel.get('id'),title,function(status,err){
            if(status=='failure'){
                alert('Error creating board ' + err);
            }
            callback();
        });
    },
    deleteBoard: function(title,callback){
        dataService.deleteBoard(loginViewModel.get('id'),title,function(status,err){
            if(status=='failure'){
                alert('Error deleting board ' + err);
            }
            callback();
        });
    }
    
});