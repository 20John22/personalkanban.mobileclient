/// <reference path="~/scripts/dataService.js"> 


var loginViewModel = kendo.observable({
        name : "test",
        email : "",
        password : "test",
        id : '',
        isEmptyName : function() {
            return this.get("name").length == 0;
        },
        isEmptyEmail : function(){
            return this.get("email").length == 0;
        },
        isEmptyPassword: function(){
            return this.get("password").length == 0;
        },
        login : function(callback){
            if(!this.isEmptyName() && !this.isEmptyPassword())
            {
                dataService.login(this.get("name"),this.get("password"), function(status,data){
                    if(status == 'failure'){
                        alert('Invalid credentials');
                        console.log('[Error]loginViewModel-hasValidCredentials: Invalid credentials');
                        callback(false);
                    }
                    else{
                        loginViewModel.set("id",data.uniqueId);
                        callback(true);
                    }
                });
            }
            else
            {
                var message = '';
                if(this.isEmptyName()){ message += 'Name'}
                if(this.isEmptyPassword()){ message += message == '' ? 'Password': ',Password'}
                alert(message + ' is empty');
                callback(false);
                
            }
        },
    createUser : function(callback){
        if(!this.isEmptyName() && !this.isEmptyEmail() && !this.isEmptyPassword())
        {
            dataService.createUser(this.get("name"),this.get('email'),this.get("password"), function(status,data){
                if(status == 'failure'){
                    alert('Failed to create user');
                    console.log('[Error]loginViewModel-hasValidCredentials: Invalid credentials');
                    callback(false);
                }
                else{
                    callback(true);
                }
            });
        }
        else
        {
            var message = '';
            if(this.isEmptyName()){ message += 'Name'}
            if(this.isEmptyEmail()){ message += message == '' ? 'Email' : ',Email'}
            if(this.isEmptyPassword()){ message += message == '' ? 'Password': ',Password'}
            alert(message + ' is empty');
            callback(false);
        }
    }
});