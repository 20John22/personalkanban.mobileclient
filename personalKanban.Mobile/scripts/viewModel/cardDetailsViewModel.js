/// <reference path="~/scripts/dataService.js"> 
/// <reference path="~/scripts/viewModel/loginViewModel.js"> 
/// <reference path="~/scripts/viewModel/boardViewModel.js"> 
var cardDetailsViewModel = new kendo.observable({
    cardName : '',
    cardDetails1: '',
    cardDetails2: '',
    getCardDetails: function(cardId){
        dataService.getCardDetails(loginViewModel.get("id"),boardViewModel.get("boardTitle"), cardId, function(data,status){
            cardDetailsViewModel.set("cardName",data.cardName);
            cardDetailsViewModel.set("cardDetails1",data.cardDetails1);
            cardDetailsViewModel.set("cardDetails2",data.cardDetails2);
        });
      
    },
    addCard: function(tableName,callback){
         var cardId = Math.floor(Math.random()*100000);
         cardObject = {
            cardId : cardId,
            cardName: this.get("cardName"),
            cardDetails1 : this.get("cardDetails1"),
            cardDetails2 : this.get("cardDetails2")
        }
       
        boardViewModel.tables[boardViewModel.currentPage()].cards.push({ id:cardId,name: cardObject.cardName });
        dataService.updateTables(loginViewModel.get("id"),boardViewModel.get("boardTitle"),boardViewModel.get("tables"), function(data,status){
            dataService.addCard(loginViewModel.get("id"),boardViewModel.get("boardTitle"),tableName,cardObject,function(){
                callback();
            });
        });
    }
        
});