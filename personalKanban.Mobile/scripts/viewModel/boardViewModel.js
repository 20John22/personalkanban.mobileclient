/// <reference path="~/scripts/dataService.js"> 
/// <reference path="~/scripts/viewModel/loginViewModel.js"> 
/// <reference path="~/scripts/viewModel/boardViewModel.js"> 
var boardViewModel = new kendo.observable({
    boardTitle : '',
    tables: [
        {   title: "Example 1xxx",
                    cards: [
                        { id: 10, name: "A fsafdas" },
                        { id: 4, name: "B fadsfa" }
                            ]
                },
                {   title: "Example 2xxx",
                    cards: [
                        { id: 8, name: "D fadsafd" },
                        { id: 5, name: "E fasdfsd" }
                            ]
                }
    ],
    currentPage: function(){
        return $("#boardSurface").data("kendoMobileScrollView").page;
    },
    getTables : function(){
        dataService.getTables(loginViewModel.get("id"),this.get("boardTitle"), function(data,status){
             boardViewModel.set('tables', data); 
            
            var encodingTemplate = kendo.template($('#tables-template').html());
            var content = kendo.render(encodingTemplate, data);
             $("#boardSurface").data("kendoMobileScrollView").content(content);
        
           
        });
      
    },
    moveElement : function(targetId,targetPage,moveBehindId,moveBehindPageNumber){
       
        if(targetId == moveBehindId && targetPage == moveBehindPageNumber){ return; }
        
        var elementToMove = this.tables[targetPage].cards[targetId];
        if(moveBehindId == -1) // move to top
            moveBehindId = 0;
        else
            moveBehindId = moveBehindId + 1; //add 1 position so we move behand this element
        this.tables[moveBehindPageNumber].cards.splice(moveBehindId,0,elementToMove);
    
        if(targetId > moveBehindId && targetPage == moveBehindPageNumber)  
            targetId = targetId + 1; // in case the element was inserted befor targeted element
        this.tables[targetPage].cards.splice(targetId,1);
        
        
        var encodingTemplate = kendo.template($('#tables-template').html());
        var content = kendo.render(encodingTemplate,   this.tables);
        $("#boardSurface").data("kendoMobileScrollView").content(content);
        dataService.updateTables(loginViewModel.get("id"),this.get("boardTitle"),this.get("tables"), function(data,status){
                
             
        });
    }
  
    
});