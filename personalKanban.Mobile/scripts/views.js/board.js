function sleep(milliseconds) {
    var start = new Date().getTime();
    while ((new Date().getTime() - start) < milliseconds){
    // Do nothing
    }
}

var timeout;
function movePageForward(){
    if(timeout) {
        return;
        //   clearTimeout(timeout);
        
        // timeout = null;
    }
    
    timeout = setTimeout(forward, 1000);
}
function movePageBackward(){
    if(timeout) {
        return;
        //clearTimeout(timeout);
        
        //timeout = null;
    }
    
    timeout = setTimeout(backward, 1000);
}
function forward() { 
    var tablesSwitcher = $("#boardSurface").data("kendoMobileScrollView");
    tablesSwitcher.scrollTo(tablesSwitcher.page + 1);
    clearTimeout(timeout); // this way will not run infinitely
    timeout = null;
}
function backward() { 
    var tablesSwitcher = $("#boardSurface").data("kendoMobileScrollView");
    tablesSwitcher.scrollTo(tablesSwitcher.page - 1);
    clearTimeout(timeout); // this way will not run infinitely
    timeout = null;
}

var currY = 0;
var currX = 0;
function draggableOnDrag(e) {
    $('#dragedElement').show();
    currX = e.x.client;
    currY =  e.y.client;
  
    if(e.x.client < 0 + 20)
    {
        // var tablesSwitcher = $("#boardSurface").data("kendoMobileScrollView");
        // tablesSwitcher.scrollTo(tablesSwitcher.page - 1); 
         movePageBackward();
        
    }
    if(e.x.client > $(window).width() - 20)
    {
        movePageForward();
    }
    
    displayShadow(currX,currY);
}

function getPageNumber(page){
    var aElements = page.parentNode.parentNode.parentNode.parentNode.getElementsByTagName("ul");
    var aElementsLength = aElements.length;
        
    var index = -1;
    for (var i = 0; i < aElementsLength; i++)
    {
        if (aElements[i] == page)
        {
           index = i;
           return index;
        }
    }
}
function getElementIndex(element){
    var aElements = element.parentNode.parentNode.getElementsByTagName("p");
    var aElementsLength = aElements.length;
        
    var index = -1;
    for (var i = 0; i < aElementsLength; i++)
    {
        if (aElements[i] == element)
        {
           index = i;
           return index;
        }
    }
}

function displayShadow(currX,currY){
    
    
    $('.shadow').removeClass('shadow');
    
    $('#dragedElement').hide();
    var element = document.elementFromPoint(currX, currY);
    $('#dragedElement').show();
    
    var isDraggable = $(element).hasClass('draggable');
    if(isDraggable)
    {
        var shadow = $(element).parent();
        shadow.next().addClass("shadow");
    }
    var isOverTopElement = $(element).hasClass('topHidden');
    if(isOverTopElement)
    {
        var shadow = $(element).parent();
        $($(shadow.children()[2]).prev()).addClass('shadow');
      //  shadow.next().addClass("shadow");
    }
}
function draggableOnDragEnd(e){
    $('.shadow').removeClass('shadow');
    $(e.currentTarget).show();
      $(e).addClass('draggable2');
}


function onHoldAccepted(e){
    var el = document.getElementById(e.id);
    $(e.children[0]).addClass('draggable2');
     $(e.children[0]).addClass('shadow2');

}

var lastTouchId = 0;
var timeoutDoubleTouch = 0;
var timeoutId = 0;
    
$('.clear-timeout').live('touchend touchmove',function() {
     clearTimeout(timeoutId); 
});

function onHoldBtn(e,id)
{
    console.log('click');
    if(id === lastTouchId){
       //check dobule touch
       var now = new Date().getTime();
       var timesince = now - timeoutDoubleTouch;
       if((timesince < 600) && (timesince > 0)){
          clearTimeout(timeoutId);
          app.navigate("#cardDetails?card=" + id); 
          return;
       }
    }
    
    lastTouchId = id;
    timeoutDoubleTouch = new Date().getTime();
    timeoutId = setTimeout(function(){ onHoldAccepted(e); }, 1000);

}

function draggableOnDragStart(e){
    $('.shadow').removeClass('shadow');
    $(e.currentTarget).hide();
}
var dragable;

function mobileListViewDataBindInitGrouped() {
    dragable = $('#boardSurface').kendoDropTarget({
       drop: function(e) {
           $('#dragedElement').hide();
           $(e.currentTarget).show();
           var element = document.elementFromPoint(currX, currY);
           var draggedElementIndex = getElementIndex(e.draggable.currentTarget[0]);
           var draggedPageIndex = getPageNumber(e.draggable.currentTarget[0].parentNode.parentNode);
           var insertAfterTargetId = getElementIndex(element);
           var tablesSwitcher = $("#boardSurface").data("kendoMobileScrollView");
           var currentPage = tablesSwitcher.page;
           if(insertAfterTargetId == null && $(element).hasClass('topHidden')){
               
               insertAfterTargetId = -1;
           }
           if(draggedElementIndex!=null && draggedPageIndex != null && insertAfterTargetId != null && currentPage !=null){
               boardViewModel.moveElement(draggedElementIndex,draggedPageIndex,insertAfterTargetId,currentPage);
           }
           else{
               $('#dragedElement').show();
           }
       }
   }).kendoDraggable({
        filter: ".draggable2",
        hint: function(e) {
            return $('<div id="dragedElement" class="table-col shadow" style="width: 77%; color: white;">' + e.html() + '</div>');
        },
        drag: draggableOnDrag,
        dragstart: draggableOnDragStart,
        dragend: draggableOnDragEnd
    });
    
  
      /*
    
    $('.draggable').kendoTouch({
        surface: $("#boardSurface"),
        drag: function(e) {
            console.log("you dragged a list item");
        }
    });
    */
}
      